﻿using UnityEngine;

public class StarManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject star;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    GameObject spawnStar;
    float timeCount;

    void Update()
    {
        //only happen when spawnstar is null
        timeCount += Time.deltaTime;

        if(spawnTime < timeCount)
        {
            Debug.Log("spawn");
            Spawn();
            timeCount = 0;
        }
    }


    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Debug.Log(spawnStar);
        if(spawnStar == null)
        {
            spawnStar = Instantiate(star, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }
        
    }
}
