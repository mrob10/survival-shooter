﻿using UnityEngine;

public class InstaKillManager : MonoBehaviour
{
    public PlayerShooting dps;
    public PlayerHealth playerHealth;
    public GameObject instaKill;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    GameObject spawnBullet;
    float timeCount;

    void Update()
    {
        //only happen when spawnstar is null
        timeCount += Time.deltaTime;

        if (spawnTime < timeCount)
        {
            Spawn();
            timeCount = 0;
        }
    }


    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        
        if (spawnBullet == null)
        {
            spawnBullet = Instantiate(instaKill, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }

    }
}
